(
(SynthDef(\monkdrone, {
	|out = 1, freq = 55, amp = 1, freqMod = 0.95, width = 0.95, lfospeed = 1, lfodepth = 0.04|
	var aBass = Vowel(\a, \bass);
	var lfoed = 2 * freq * SinOsc.kr(lfospeed, 0, lfodepth, 1);
	var sine = SinOsc.ar(freq, 0, amp);
	var pulseBass = Pulse.ar(lfoed, 0.05);
	var voiceBass =  BPFStack.ar(pulseBass, aBass, freqMod, amp, width);
	var sig = voiceBass + (sine * 0.3);
	sig = BLowPass4.ar(sig, 440 * 8);
	Out.ar(out, sig);
}).add();
);

(SynthDef(\kontrolgod, {
	|out, start = 1, end = 0, dur = 12|
	var env = XLine.kr(start, end, dur, doneAction: 2);
	XOut.kr(out, 1, env);
}).add()
);

(
~getTriggerer = {
	|out, minValue, maxValue, minDur, maxDur, identifier|
	Routine({
		var start = 1;
		loop{
			var end = rrand(minValue, maxValue.asFloat);
			var dur = rrand(minDur, maxDur.asFloat);
			var crossfadeDur = rrand(minDur, dur);
			Synth(\kontrolgod, [\out, out, \start, start, \end, end, \dur, dur]);
			start = end;
			identifier.post;
			": ".post;
			start.postln;
			dur.wait;
		}
	})
});
)
