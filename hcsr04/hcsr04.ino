#include <NewPing.h>

NewPing sonar(11, 13, 200);

void setup() {
  Serial.begin(115200);
}

void loop() {
  delay(25);
  Serial.println(sonar.ping_cm());
}
